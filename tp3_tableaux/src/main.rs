#[macro_use]
extern crate text_io;
mod array_utils;

use array_utils::*;

pub const TAB_SIZE: usize = 10;

fn main() {
    let mut tab = [0i32; TAB_SIZE];
    println!("Here is the tab filled randomly : ");
    fill_rnd(&mut tab);
    println!("{:?}", tab);

    println!("The smallest value is : {}", find_smallest(&tab));

    move_biggest(&mut tab);
    println!("Swap biggest and last : {:?}", tab);

    println!("Type a number to find : ");
    let num_to_find: i32 = read!();
    let found_index = find(&tab, num_to_find);
    println!("It is at the index : {}", found_index);

    println!("Average : {:.2}", average(&tab));

    println!("Variance : {:.2}", variance(&tab));

    println!("Median : {}", median(&tab));
}
