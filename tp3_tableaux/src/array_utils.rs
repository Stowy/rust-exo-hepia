use crate::TAB_SIZE;
use rand::thread_rng;
use rand::Rng;

pub fn fill_rnd(tab: &mut [i32; TAB_SIZE]) {
    let mut rng = thread_rng();
    for item in tab {
        *item = rng.gen_range(-10..10);
    }
    // rng.fill(tab);
}

pub fn find_smallest(tab: &[i32; TAB_SIZE]) -> &i32 {
    tab.iter().min().expect("Error finding smallest value")
}

pub fn move_biggest(tab: &mut [i32; TAB_SIZE]) {
    // Get value of biggest  number
    let biggest = *tab.iter().max().expect("Error finding biggest value");

    // Find pos of biggest
    let biggest_pos = tab.iter().position(|i| *i == biggest).unwrap();

    // Find pos of last elem
    let last_pos = tab.len() - 1;

    // Swap items
    tab.swap(biggest_pos, last_pos);
}

pub fn find(tab: &[i32; TAB_SIZE], val: i32) -> usize {
    tab.iter().position(|i| *i == val).unwrap()
}

pub fn average(tab: &[i32; TAB_SIZE]) -> f32 {
    tab.iter().sum::<i32>() as f32 / tab.len() as f32
}

pub fn variance(tab: &[i32; TAB_SIZE]) -> f32 {
    let mut sum: f32 = 0.0;
    let avg = average(&tab);

    for i in tab {
        sum += (*i as f32 * avg).powi(2);
    }

    (1.0 / tab.len() as f32) * sum
}

pub fn median(tab: &[i32; TAB_SIZE]) -> i32 {
    let mut copy = *tab;
    copy.sort();
    let mid_index = copy.len() / 2;

    copy[mid_index]
}
