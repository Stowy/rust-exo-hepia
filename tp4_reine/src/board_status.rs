#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum BoardStatus {
    Empty,
    CanMoveTo,
    Queen,
}
