#[macro_use]
extern crate text_io;
mod board_status;

use board_status::BoardStatus;

const BOARD_SIZE: usize = 8;

fn main() {
    let mut board = [[BoardStatus::Empty; BOARD_SIZE]; BOARD_SIZE];
    let unwraped_coords;
    loop {
        // Read the coordinates from the user
        println!("Please input x coordinates for the queen [a-h] :");
        let x: char = read!();
        println!("Please input y coordinates for the queen [1-8] :");
        let y: usize = read!();

        let converted_coords = convert_coord(x, y);
        if converted_coords.is_some() {
            unwraped_coords = converted_coords.unwrap();
            break;
        }
    }

    board[unwraped_coords.0][unwraped_coords.1] = BoardStatus::Queen;

    compute_queen_movement(&mut board);

    print_board(&board);
}

fn convert_coord(x: char, y: usize) -> Option<(usize, usize)> {
    let x_value = x as usize - 97;
    let y_value = 7 - (y - 1);
    if x_value >= BOARD_SIZE || y_value >= BOARD_SIZE {
        None
    } else {
        Some((x_value, y_value))
    }
}

fn compute_queen_movement(board: &mut [[BoardStatus; BOARD_SIZE]; BOARD_SIZE]) {
    let mut queen_coords: (usize, usize) = (0, 0);
    for (x, row) in board.iter().enumerate() {
        for (y, col) in row.iter().enumerate() {
            if *col == BoardStatus::Queen {
                queen_coords = (x, y);
            }
        }
    }

    for y in 0..BOARD_SIZE {
        for x in 0..BOARD_SIZE {
            let horizontal_vertial_check = x == queen_coords.0 || y == queen_coords.1;
            let diff_x = (x as i32 - queen_coords.0 as i32).abs();
            let diff_y = (y as i32 - queen_coords.1 as i32).abs();
            let diagonal_check = diff_x == diff_y;
            let is_queen = x == queen_coords.0 && y == queen_coords.1;
            if (horizontal_vertial_check || diagonal_check) && !is_queen {
                board[x][y] = BoardStatus::CanMoveTo;
            }
        }
    }
}

fn print_board(board: &[[BoardStatus; BOARD_SIZE]; BOARD_SIZE]) {
    for y in 0..BOARD_SIZE {
        print!("{} ", 8 - y);
        for x in 0..BOARD_SIZE {
            match board[x][y] {
                BoardStatus::Empty => print!(". "),
                BoardStatus::CanMoveTo => print!("* "),
                BoardStatus::Queen => print!("Q "),
            }
        }
        println!("");
    }
    println!("  a b c d e f g h");
}
