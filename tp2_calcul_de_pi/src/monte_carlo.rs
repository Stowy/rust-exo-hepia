use rand::thread_rng;
use rand::Rng;

const RADIUS: f64 = 1.0;

pub fn pi_monte_carlo(iterations: i64) -> f64 {
    let mut rng = thread_rng();
    let mut k: f64 = 0.0;

    for _ in 0..iterations {
        // If the hypothenuse of the coordinates is smaller than the radius, the point is in the circle
        let x = rng.gen_range(-RADIUS..RADIUS);
        let y = rng.gen_range(-RADIUS..RADIUS);
        let hypothenuse = (x * x + y * y).sqrt();
        if hypothenuse <= RADIUS {
            k += 1.0;
        }
    }

    let ratio = k / iterations as f64;
    ratio * 4.0
}
