pub trait Root {
    fn nth_root(&self, a: Self) -> Self;
}

impl Root for f64 {
    // From https://rosettacode.org/wiki/Nth_root#Rust
    fn nth_root(&self, n: Self) -> Self {
        let p = 1e-9_f64;
        let mut x0 = n / self;
        loop {
            let x1 = ((self - 1.0) * x0 + n / f64::powf(x0, self - 1.0)) / self;
            if (x1 - x0).abs() < (x0 * p).abs() {
                return x1;
            };
            x0 = x1
        }
    }
}
