use rand::thread_rng;
use rand::Rng;

pub fn pi_buffons_needle(iterations: i64) -> f64 {
    let mut rng = thread_rng();

    // Width of the plank
    let a: f64 = 2.0;

    // Half of the length of the needle, lambda < a
    let lambda: f64 = 1.0;

    let mut sum_crossed: f64 = 0.0;

    for _ in 0..iterations {
        // Distance of the center of the needle to the plank
        let x: f64 = rng.gen_range(0.0..a);

        // Angle of the needle to the plank
        let theta = rng.gen_range(0.0..(std::f64::consts::PI / 2.0));

        // Check if the needle crosses a line
        if x <= lambda * theta.cos() {
            sum_crossed += 1.0;
        }
    }

    let probability = sum_crossed / iterations as f64;

    let pi = (2.0 * lambda) / (a * probability);

    pi
}
