use crate::root::Root;

pub fn pi_series_numeriques_1(iterations: i64) -> f64 {
    // Make sure we have enough iterations
    if iterations <= 1 {
        return -1f64;
    }
    let mut sum = 0f64;
    for i in 1..iterations {
        let float_i = i as f64;
        let pow = float_i * float_i * float_i * float_i;
        sum += 1f64 / pow;
    }
    (sum * 90.0).nth_root(4f64)
}
pub fn pi_series_numeriques_2(iterations: i64) -> f64 {
    // Make sure we have enough iterations
    if iterations <= 1 {
        return -1.0;
    }
    let mut sum = 0f64;
    for i in 1..iterations {
        let float_i = i as f64;
        // Parenthesis around the one because otherwise the minus is applied to everything >:(
        let pow = (-1f64).powf(float_i + 1.0);
        sum += pow / (float_i * float_i)
    }
    (sum * 12.0).sqrt()
}
pub fn pi_series_numeriques_3(iterations: i64) -> f64 {
    // Make sure we have enough iterations
    if iterations <= 1 {
        return -1.0;
    }
    let mut sum = 1f64;
    for i in 1..iterations {
        let float_i = i as f64;
        let a = (2.0 * float_i) / (2.0 * float_i - 1.0);
        let b = (2.0 * float_i) / (2.0 * float_i + 1.0);
        sum *= a * b;
    }
    sum * 2.0
}
