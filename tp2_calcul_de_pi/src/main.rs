#[macro_use]
extern crate text_io;
mod buffons_needle;
mod monte_carlo;
mod num_series;
pub mod root;

use buffons_needle::*;
use monte_carlo::*;
use num_series::*;

type PiCompute = fn(i64) -> f64;

fn main() {
    println!("Let's print PI !");
    println!("Pick a method : ");
    println!("1 : séries numériques");
    println!("2 : monte carlo");
    println!("3 : buffons needle");

    let input: i32 = read!();

    match input {
        2 => print_monte_carlo(),
        3 => print_buffons_needle(),
        _ => print_series_numerique(),
    }
}

fn print_pi(func: PiCompute, iter: i64, name: &str) {
    println!("Pi {} : ", name);
    let pi = func(iter);
    let pi_diff = std::f64::consts::PI - pi;
    println!("Good PI : {}", std::f64::consts::PI);
    println!("Computed PI : {}", pi);
    println!("PI diff : {}", pi_diff);
}

fn print_series_numerique() {
    print_pi(pi_series_numeriques_1, 50, "séries numériques 1");

    print_pi(pi_series_numeriques_2, 1000, "séries numériques 2");

    print_pi(pi_series_numeriques_3, 1000, "séries numériques 3");
}

fn print_monte_carlo() {
    print_pi(pi_monte_carlo, 10000, "monte carlo");
}

fn print_buffons_needle() {
    print_pi(pi_buffons_needle, 1000000, "buffons needle");
}
