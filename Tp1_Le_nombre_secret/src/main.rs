#[macro_use]
extern crate text_io;
extern crate rand;

use rand::thread_rng;
use rand::Rng;

fn main() {
    // Create random generator
    let mut rng = thread_rng();

    println!("What is the max number you want ?");

    // Read max number
    let max: i32 = read!();

    // Generate random number
    let num_to_guess: i32 = rng.gen_range(0..max);

    loop {
        println!("Guess a number : ");
        // Read guessed number
        let guessed_num: i32 = read!();

        if guessed_num < num_to_guess {
            println!("Guessed number is too small.");
        } else if guessed_num > num_to_guess {
            println!("Guessed number is too big.");
        } else if guessed_num == num_to_guess {
            println!("Guessed number is right!");
            break;
        }
    }
}
